# Publish Module
- php artisan vendor:publish --provider="AdminAuth\AdminAuthServiceProvider" --tag="config"
- php artisan vendor:publish --provider="AdminAuth\AdminAuthServiceProvider" --tag="views"
- php artisan vendor:publish --provider="AdminAuth\AdminAuthServiceProvider" --tag="migrations"
- php artisan vendor:publish --provider="AdminAuth\AdminAuthServiceProvider" --tag="assets"
