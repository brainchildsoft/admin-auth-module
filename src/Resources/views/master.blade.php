<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <script src="https://cdn.tailwindcss.com"></script>
    <!-- tw-elements Link -->
    <link rel="stylesheet" href="{{ asset('brainchildsoft/tw-elements/index.min.css') }}" />
    <!-- Boxiocns Link -->
    <link href='{{ asset('brainchildsoft/boxicons/boxicons.min.css') }}' rel='stylesheet'>
    <!-- font awesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- brainchild admin Link -->
    @stack('before-styles')
    <link rel="stylesheet" href="{{ asset('brainchildsoft/css/main.css') }}">
    @stack('after-styles')
</head>
<body>
@yield('content')

@include('backend.includes.footer')
<script src="{{ asset('brainchildsoft/tw-elements/index.min.js') }}"></script>
@stack('before-scripts')

@stack('after-scripts')
</body>
</html>
