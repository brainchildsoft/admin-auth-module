@if ($errors->any())
    <div class="alert alert-danger">
        <div>
            <div class="font-medium text-red-600">{{ __('Whoops! Something went wrong.') }}</div>

            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
@if ($error = Session::get('validation'))
    <div class="alert alert-danger">
        <div class="font-medium text-red-600">{{ __('Whoops! Something went wrong.') }}</div>
        <ul class="mt-3 list-disc list-inside text-sm text-red-600">
            <li>{{ $error }}</li>
        </ul>
    </div>
@endif

@if ($error = Session::get('error'))
    <div class="alert alert-danger">
        <div class="font-medium text-red-600">{{ __('Whoops! Something went wrong.') }}</div>
        <ul class="mt-3 list-disc list-inside text-sm text-red-600">
            <li>{{ $error }}</li>
        </ul>
    </div>
@endif
{{-- /*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/ --}}
