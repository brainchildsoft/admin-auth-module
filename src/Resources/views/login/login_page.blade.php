@extends(config('adminauth.layout'))

@section('title', config('adminauth.name'))

@section('content')
    <section class="bg-gradient-to-r from-[#001527] to-[#000a27]">
        <div class="h-auto mix-blend-darken:h-screen">
            <div class="container h-full px-6 py-6 mx-auto md:py-0">
                <div class="flex flex-col items-center justify-center h-auto gap-4 md:gap-6 md:h-full md:flex-row lg:gap-10">
                    <div class="w-full overflow-hidden md:h-screen h-72 md:w-6/12 lg:w-7/12 2xl:w-8/12">
                        <div id="carouselExampleCaptions" class="relative h-full overflow-hidden carousel slide"
                             data-bs-ride="carousel">
                            <div class="absolute bottom-0 left-0 right-0 flex justify-center p-0 mb-4 carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                                        aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                        aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                        aria-label="Slide 3"></button>
                            </div>
                            <div class="relative w-full h-full overflow-hidden carousel-inner">
                                <div class="relative float-left w-full h-full carousel-item active">
                                    <div class="relative object-cover h-full overflow-hidden bg-no-repeat bg-cover"
                                         style="background-position: 50%;">
                                        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(123).jpg"
                                             class="block object-cover w-full h-full" />
                                        <div
                                            class="absolute top-0 bottom-0 left-0 right-0 w-full h-full overflow-hidden bg-fixed bg-black opacity-50">
                                        </div>
                                    </div>
                                    <div class="absolute hidden text-center carousel-caption md:block">
                                        <h5 class="text-xl">First slide label</h5>
                                        <p>Some representative placeholder content for the first slide.</p>
                                    </div>
                                </div>
                                <div class="relative float-left w-full h-full carousel-item">
                                    <div class="relative h-full overflow-hidden bg-no-repeat bg-cover" style="background-position: 50%;">
                                        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(124).jpg"
                                             class="block object-cover w-full h-full" />
                                        <div
                                            class="absolute top-0 bottom-0 left-0 right-0 w-full h-full overflow-hidden bg-fixed bg-black opacity-50">
                                        </div>
                                    </div>
                                    <div class="absolute hidden text-center carousel-caption md:block">
                                        <h5 class="text-xl">Second slide label</h5>
                                        <p>Some representative placeholder content for the second slide.</p>
                                    </div>
                                </div>
                                <div class="relative float-left w-full h-full carousel-item">
                                    <div class="relative h-full overflow-hidden bg-no-repeat bg-cover" style="background-position: 50%;">
                                        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(125).jpg"
                                             class="block object-cover w-full h-full" />
                                        <div
                                            class="absolute top-0 bottom-0 left-0 right-0 w-full h-full overflow-hidden bg-fixed bg-black opacity-50">
                                        </div>
                                    </div>
                                    <div class="absolute hidden text-center carousel-caption md:block">
                                        <h5 class="text-xl">Third slide label</h5>
                                        <p>Some representative placeholder content for the third slide.</p>
                                    </div>
                                </div>
                            </div>
                            <button
                                class="absolute top-0 bottom-0 left-0 flex items-center justify-center p-0 text-center border-0 carousel-control-prev hover:outline-none hover:no-underline focus:outline-none focus:no-underline"
                                type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                <span class="inline-block bg-no-repeat carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button
                                class="absolute top-0 bottom-0 right-0 flex items-center justify-center p-0 text-center border-0 carousel-control-next hover:outline-none hover:no-underline focus:outline-none focus:no-underline"
                                type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                <span class="inline-block bg-no-repeat carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
<<<<<<< HEAD
                    <div style="background-color: rgba(106, 106, 106, 0.2);" class="w-full shadow rounded-md p-4 sm:p-6 text-white md:w-6/12 lg:w-5/12 2xl:w-4/12">
                        <form action="{{ route('backend.login') }}" method="post">
                            @csrf
                            <h2 class="py-0 text-5xl font-medium sm:py-4">Login</h2>
                            @include('adminauth::partials.message')
                            <div class="mx-auto">
                                <div class="py-2">
                                    <label for="username" class="px-1 font-medium">Username/email</label>
                                    <input
                                        id="username"
                                        placeholder="Username/Email"
                                        type="text"
                                        name="{{ config('adminauth.auth_columns.username')  }}"
                                        class="block w-full px-3 text-black py-3 mt-2 placeholder-gray-500 bg-white border border-gray-300 rounded-lg text-md focus:placeholder-gray-700 focus:outline-none">
                                </div>
                                <div class="py-2">
                                    <label for="password" class="px-1 font-medium">Password</label>
                                    <div class="relative">
                                        <input id="password"
                                               placeholder="8+ character"
                                               name="{{ config('adminauth.auth_columns.password') }}"
                                               type="password"
                                               class="block w-full text-black px-3 py-3 mt-2 placeholder-gray-500 bg-white border border-gray-300 rounded-lg focus:placeholder-gray-700 text-md focus:outline-none">
                                        <div class="absolute inset-y-0 right-0 flex items-center pr-3 text-xl leading-5">
                                            <i id="toggle-password" class="fa-solid fa-eye-slash"></i>
                                        </div>
=======
                </div>
                <div style="background-color: rgba(106, 106, 106, 0.2);" class="w-full shadow rounded-md p-4 sm:p-6 text-white md:w-6/12 lg:w-5/12 2xl:w-4/12">
                    <form action="{{ route('backend.login') }}" method="post">
                        @csrf
                        <h2 class="py-0 text-5xl font-medium sm:py-4">Login</h2>
                        @include('adminauth::partials.message')
                        <div class="mx-auto">
                            <div class="py-2">
                                <label for="username" class="px-1 font-medium">Username/email</label>
                                <input
                                    id="username"
                                    placeholder="Username/Email"
                                    type="text"
                                    name="{{ config('adminauth.auth_columns.username')  }}"
                                    class="block w-full px-3 text-black py-3 mt-2 placeholder-gray-500 bg-white border border-gray-300 rounded-lg text-md focus:placeholder-gray-700 focus:outline-none">
                            </div>
                            <div class="py-2">
                                <label for="password" class="px-1 font-medium">Password</label>
                                <div class="relative">
                                    <input id="password"
                                           placeholder="8+ character"
                                           name="{{ config('adminauth.auth_columns.password') }}"
                                           type="password"
                                           class="block w-full text-black px-3 py-3 mt-2 placeholder-gray-500 bg-white border border-gray-300 rounded-lg focus:placeholder-gray-700 text-md focus:outline-none">
                                    <div class="absolute inset-y-0 right-0 flex items-center pr-3 text-xl leading-5">
                                        <i id="toggle-password" class="fa-solid fa-eye-slash"></i>
>>>>>>> origin/klikmoris/features/full-booking-feature
                                    </div>
                                </div>
                                <!--                            <div class="flex justify-between">
                                                                <label class="flex items-center gap-2 my-4 font-bold text-white">
                                                                    <input type="checkbox" name="remember" value="1" class="w-5 h-5 leading-loose">
                                                                    <span> Remember Me </span>
                                                                </label>
                                                                <label class="block my-4 font-bold text-white">
                                                                    <a href="#" class="tracking-tighter border-b-2 border-gray-200 cursor-pointer hover:border-gray-400">
                                                                        <span>Forgot Password?</span>
                                                                    </a>
                                                                </label>
                                                            </div>-->
                                <button type="submit" class="block w-full px-6 py-3 mt-3 text-lg font-semibold text-white rounded-lg shadow-xl bg-green-800 transition-all hover:bg-green-700">
                                    Login
                                </button>
                            </div>
<<<<<<< HEAD
                        </form>
                    </div>
=======
<!--                            <div class="flex justify-between">
                                <label class="flex items-center gap-2 my-4 font-bold text-white">
                                    <input type="checkbox" name="remember" value="1" class="w-5 h-5 leading-loose">
                                    <span> Remember Me </span>
                                </label>
                                <label class="block my-4 font-bold text-white">
                                    <a href="#" class="tracking-tighter border-b-2 border-gray-200 cursor-pointer hover:border-gray-400">
                                        <span>Forgot Password?</span>
                                    </a>
                                </label>
                            </div>-->
                            <button type="submit" class="block w-full px-6 py-3 mt-3 text-lg font-semibold text-white rounded-lg shadow-xl bg-green-800 transition-all hover:bg-green-700">
                                Login
                            </button>
                        </div>
                    </form>
>>>>>>> origin/klikmoris/features/full-booking-feature
                </div>
            </div>
        </div>
    </section>
@endsection

@push('after-scripts')
    <script>
        let slideIndex = 0;
        showSlides();

        function showSlides() {
            let i;
            let slides = document.getElementsByClassName("mySlides");
            let dots = document.getElementsByClassName("dot");
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > slides.length) {slideIndex = 1}
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            setTimeout(showSlides, 2000); // Change image every 2 seconds
        }

    </script>
@endpush
