<?php

namespace AdminAuth;
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/

use AdminAuth\Providers\RouteServiceProvider;
use Illuminate\Support\ServiceProvider;

class AdminAuthServiceProvider extends ServiceProvider
{

    /**
     * @var string $moduleName
     */
    protected $moduleName = 'AdminAuth';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'adminauth';


    /**
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', $this->moduleNameLower);
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishConfig();
            $this->publishMigrations();
            $this->publishViews();
            $this->publishAssets();
        }
        $this->loadViewsFrom(__DIR__.'/Resources/views', 'adminauth');
        $this->loadMigrationsFrom(__DIR__.'/Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [];
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function publishConfig(): void
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('adminauth.php'),
        ], 'config');
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function publishMigrations(): void
    {
        if (! class_exists('CreateAdminAuthTable')) {
            $this->publishes([
                __DIR__ . '/Database/Migrations/2023_01_01_000000_create_admin_auth_table.php.stub' => database_path('migrations/2023_01_01_000000_create_admin_auth_table.php'),
                __DIR__ . '/Database/Seeders/AdminAuthDatabaseSeeder.php.stub' => database_path('seeders/AdminAuthDatabaseSeeder.php'),
                // you can add any number of migrations here
            ], 'migrations');
        }
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function publishViews(): void
    {
        $viewPath = resource_path('views/bcsmodules/' . $this->moduleNameLower);
        $sourcePath =__DIR__.'/Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');
    }

    /**
     * Register assets.
     *
     * @return void
     */
    public function publishAssets(): void
    {
        $this->publishes([
            __DIR__.'/Resources/assets' => public_path($this->moduleNameLower),
        ], 'assets');
    }
}
