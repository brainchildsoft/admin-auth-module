<?php
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/

namespace AdminAuth\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthService
{
    /**
     * @param Request $request
     * @return array
     */
    public function authLogin(Request $request): array
    {
        $credentials = [
            $this->username() => $request->get(config('adminauth.auth_columns.username')),
            config('adminauth.auth_columns.password') => $request->get(config('adminauth.auth_columns.password')),
        ];

        if (Auth::guard(config('adminauth.guard'))->attempt($credentials)){
            $request->session()->regenerate();
            $request->session()->regenerateToken();
            return $request->user(config('adminauth.guard'))->toArray();
        }else{
            throw new \Exception('Username/email and password not match');
        }
    }

    /**
     * Check either username or email.
     * @return string
     */
    private function username()
    {
        $identity  = request()->get(config('adminauth.auth_columns.username'));
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL)
            ? config('adminauth.auth_columns.email')
            : config('adminauth.auth_columns.username');
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }
}
