<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \AdminAuth\Http\Controllers\AdminAuthController;

Route::get('admin-auth/health-check', function (){
   dd('ok');
});

Route::group(['as'=>'backend.'], function (){
    Route::get('login', 'AdminAuthController@loginPageShow')->name('login');
    Route::post('login', 'AdminAuthController@login');
    Route::post('logout', 'AdminAuthController@logout')->name('logout');
});

