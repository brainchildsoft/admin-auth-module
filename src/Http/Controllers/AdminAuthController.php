<?php
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/

namespace AdminAuth\Http\Controllers;

use AdminAuth\Enums\AdminAuthEnum;
use AdminAuth\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use AdminAuth\Services\AdminAuthService;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;

class AdminAuthController extends Controller
{
    public function __construct(private AdminAuthService $authService)
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginPageShow()
    {
        if (!config('adminauth.can_login')){
            return abort(Response::HTTP_NOT_FOUND, 'Page not found');
        }
        return view('adminauth::login.login_page');
    }

    public function login(LoginRequest $request)
    {
        if (!config('adminauth.can_login')){
            return abort(Response::HTTP_SERVICE_UNAVAILABLE, 'Admin auth login service not available');
        }
        try {
            $this->authService->authLogin($request);
            return redirect(config('adminauth.login_success_url'));
        }catch (UnauthorizedException $exception){
            Log::info(AdminAuthEnum::ERROR_LOG_KEY.$exception->getMessage().'-'.$exception->getFile().'-'.$exception->getLine());
            return redirect()->back()->with('error', 'Username/email and password not match');
        }catch (\Throwable $throwable){
            Log::info(AdminAuthEnum::ERROR_LOG_KEY.$throwable->getMessage().'-'.$throwable->getFile().'-'.$throwable->getLine());
            return redirect()->back()->with('error', 'Something Wrong. Try again later');
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
//        $request->user(config('adminauth.guard'))->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/')->with('success', 'Logout Successfully');
    }
}
