<?php
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/

namespace AdminAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            config('adminauth.auth_columns.username')=>['required', 'string'],
            config('adminauth.auth_columns.password')=>['required', 'min:8'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            config('adminauth.auth_columns.username').'.required' => 'Username/Email is required',
            config('adminauth.auth_columns.username').'.string' => 'Username/Email must be string',
            config('adminauth.auth_columns.password').'.required' => 'Password is required',
            config('adminauth.auth_columns.password').'.min' => 'Password min 8 characters',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
