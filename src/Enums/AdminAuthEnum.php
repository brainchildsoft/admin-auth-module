<?php
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/

namespace AdminAuth\Enums;

class AdminAuthEnum
{
    public const ERROR_LOG_KEY='adminauth:error:';
}
