<?php
/*
* Author: Arup Kumer Bose
* Email: arupkumerbose@gmail.com
* Company Name: Brainchild Software <brainchildsoft@gmail.com>
*/
return [
    'name' => 'AdminAuth',

    'layout'=>'adminauth::master',
    'guard'=>'adminauth',

    'login_success_url'=>'/dashboard',
    'register_success_url'=>'/dashboard',
    'can_register'=>true,
    'can_login'=>true,

    'auth_table'=>'admins',
    'prefix'=>'admin',
    'web_middleware'=>['web'],
    'api_middleware'=>['api'],

    'auth_columns'=>[
        'username'=>'username',
        'email'=>'email',
        'password'=>'password'
    ]
];
